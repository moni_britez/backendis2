
CREATE SEQUENCE public.rol_id_rol_seq;

CREATE TABLE public.rol (
                id_rol INTEGER NOT NULL DEFAULT nextval('public.rol_id_rol_seq'),
                nombre_rol VARCHAR(10) NOT NULL,
                descripcion VARCHAR(250),
                CONSTRAINT rol_pk PRIMARY KEY (id_rol)
);


ALTER SEQUENCE public.rol_id_rol_seq OWNED BY public.rol.id_rol;

CREATE SEQUENCE public.usuario_id_usuario_seq;

CREATE TABLE public.usuario (
                id_usuario INTEGER NOT NULL DEFAULT nextval('public.usuario_id_usuario_seq'),
                nombre VARCHAR(50) NOT NULL,
                apellido VARCHAR(50) NOT NULL,
                correo VARCHAR(50) NOT NULL,
                usuario VARCHAR(20) NOT NULL,
                contrasenha VARCHAR(20) NOT NULL,
                telefono INTEGER NOT NULL,
                id_rol INTEGER NOT NULL,
                CONSTRAINT usuario_pk PRIMARY KEY (id_usuario)
);


ALTER SEQUENCE public.usuario_id_usuario_seq OWNED BY public.usuario.id_usuario;

CREATE SEQUENCE public.proyecto_id_proyecto_seq;

CREATE TABLE public.proyecto (
                id_proyecto INTEGER NOT NULL DEFAULT nextval('public.proyecto_id_proyecto_seq'),
                fecha_inicio DATE NOT NULL,
                fecha_fin DATE NOT NULL,
                descripcion VARCHAR(250),
                estado VARCHAR(10) NOT NULL,
                id_usuario INTEGER NOT NULL,
                CONSTRAINT proyecto_pk PRIMARY KEY (id_proyecto)
);


ALTER SEQUENCE public.proyecto_id_proyecto_seq OWNED BY public.proyecto.id_proyecto;

CREATE SEQUENCE public.sprint_id_sprint_seq;

CREATE TABLE public.sprint (
                id_sprint INTEGER NOT NULL DEFAULT nextval('public.sprint_id_sprint_seq'),
                nombre VARCHAR(50) NOT NULL,
                descripcion VARCHAR(250) NOT NULL,
                estado VARCHAR(10) NOT NULL,
                duracion INTEGER NOT NULL,
                id_usuario INTEGER NOT NULL,
                id_proyecto INTEGER NOT NULL,
                CONSTRAINT sprint_pk PRIMARY KEY (id_sprint)
);


ALTER SEQUENCE public.sprint_id_sprint_seq OWNED BY public.sprint.id_sprint;

CREATE SEQUENCE public.product_backlog_id_product_seq;

CREATE TABLE public.product_backlog (
                id_product INTEGER NOT NULL DEFAULT nextval('public.product_backlog_id_product_seq'),
                descripcion VARCHAR(100) NOT NULL,
                prioridad VARCHAR(10) NOT NULL,
                duracion INTEGER NOT NULL,
                id_sprint INTEGER NOT NULL,
                id_usuario INTEGER NOT NULL,
                encargado INTEGER NOT NULL,
                CONSTRAINT product_backlog_pk PRIMARY KEY (id_product)
);


ALTER SEQUENCE public.product_backlog_id_product_seq OWNED BY public.product_backlog.id_product;

ALTER TABLE public.usuario ADD CONSTRAINT rol_usuario_fk
FOREIGN KEY (id_rol)
REFERENCES public.rol (id_rol)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.proyecto ADD CONSTRAINT usuario_proyecto_fk
FOREIGN KEY (id_usuario)
REFERENCES public.usuario (id_usuario)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.product_backlog ADD CONSTRAINT usuario_product_backlog_fk
FOREIGN KEY (id_usuario)
REFERENCES public.usuario (id_usuario)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.product_backlog ADD CONSTRAINT usuario_product_backlog_fk1
FOREIGN KEY (encargado)
REFERENCES public.usuario (id_usuario)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.sprint ADD CONSTRAINT usuario_sprint_fk
FOREIGN KEY (id_usuario)
REFERENCES public.usuario (id_usuario)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.sprint ADD CONSTRAINT proyecto_sprint_fk
FOREIGN KEY (id_proyecto)
REFERENCES public.proyecto (id_proyecto)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.product_backlog ADD CONSTRAINT sprint_product_backlog_fk
FOREIGN KEY (id_sprint)
REFERENCES public.sprint (id_sprint)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;
