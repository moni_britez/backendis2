/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.ingesof.converter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.springframework.stereotype.Component;
import py.com.ingesof.entity.Rol;
import py.com.ingesof.request.RoleRequest;
import py.com.ingesof.response.RolePostResponse;
import py.com.ingesof.response.RoleResponse;

/**
 *
 * @author usuario
 */
@Component
public class RoleConverter {
    
    public List<RoleResponse> roleListConverter(List<Rol> rolList){
        List<RoleResponse> roleList = new ArrayList<>();
        rolList.stream().forEach(p -> {
            RoleResponse partialRole = new RoleResponse();
            partialRole.setId(p.getIdRol());
            partialRole.setName(p.getNombreRol());
            partialRole.setDescription(p.getDescripcion());
            roleList.add(partialRole);
        });
        return roleList;
    }
    
    public RoleResponse roleObjectConverter(Rol rol){
        
        RoleResponse partialRole = new RoleResponse();
        partialRole.setName(rol.getNombreRol());
        partialRole.setDescription(rol.getDescripcion());

        return partialRole;
    }
    
    public List<RolePostResponse> roleSaveConverter(Integer id){
        return Arrays.asList(new RolePostResponse(id));
    }
    
    public Rol convertReqToRole(RoleRequest request){
        Rol rol = new Rol();
        rol.setDescripcion(request.getDescription());
        rol.setNombreRol(request.getRoleName());
        rol.setIdRol(null);
        return rol;
    }
    
    public RolePostResponse convertRolToPostResponse(Rol rol){
        RolePostResponse rs = new RolePostResponse();
        rs.setIdResult(rol.getIdRol());
        return rs;
    }
    
}
