package py.com.ingesof.converter;

import org.springframework.stereotype.Component;
import py.com.ingesof.entity.Proyecto;
import py.com.ingesof.entity.Sprint;
import py.com.ingesof.entity.Usuario;
import py.com.ingesof.request.SprintRequest;

/**
 *
 * @author usuario
 */
@Component
public class SprintConverter {
    
    public Sprint modelToEntitySave(SprintRequest sprintRequest, Usuario user, Proyecto project) {
        Sprint sprint = new Sprint();
        sprint.setIdSprint(null);
        sprint.setNombre(sprintRequest.getNombre());
        sprint.setDescripcion(sprintRequest.getDescripcion());
        sprint.setEstado(sprintRequest.getEstado());
        sprint.setDuracion(sprintRequest.getDuracion());
        sprint.setIdProyecto(project);
        sprint.setIdUsuario(user);
        return sprint;
    }
    
    public Sprint modelToEntityUpdate(SprintRequest sprintRequest, Usuario user, Proyecto project) {
        Sprint sprint = new Sprint();
        sprint.setIdSprint(sprintRequest.getIdSprint());
        sprint.setNombre(sprintRequest.getNombre());
        sprint.setDescripcion(sprintRequest.getDescripcion());
        sprint.setEstado(sprintRequest.getEstado());
        sprint.setDuracion(sprintRequest.getDuracion());
        sprint.setIdProyecto(project);
        sprint.setIdUsuario(user);
        return sprint;
    }
}
