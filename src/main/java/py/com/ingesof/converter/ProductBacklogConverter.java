package py.com.ingesof.converter;

import static java.lang.reflect.Array.set;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import py.com.ingesof.entity.ProductBacklog;
import py.com.ingesof.entity.Sprint;
import py.com.ingesof.entity.Usuario;
import py.com.ingesof.request.ProductBacklogRequest;
import py.com.ingesof.response.ProductBacklogResponse;

@Component
public class ProductBacklogConverter {
    
    @Autowired
    private UserConverter userConverter;
    
    public List<ProductBacklogResponse> entityToList(List<ProductBacklog> list){
        List<ProductBacklogResponse> l = new ArrayList<>();
        list.stream().forEach(p ->{
            ProductBacklogResponse pbr = new ProductBacklogResponse();
            pbr.setIdProduct(p.getIdProduct());
            pbr.setDescripcion(p.getDescripcion());
            pbr.setPrioridad(p.getPrioridad());
            pbr.setDuracion(p.getDuracion());
            pbr.setIdSprint(p.getIdSprint());
            pbr.setIdUsuario(userConverter.converterUserObject(p.getIdUsuario()));
            pbr.setEncargado(userConverter.converterUserObject(p.getEncargado()));
            l.add(pbr);
        });
        return l;
    }
    
    public List<ProductBacklogResponse> productBacklogById(ProductBacklog pb){
        ProductBacklogResponse pbr = new ProductBacklogResponse();
        pbr.setIdProduct(pb.getIdProduct());
        pbr.setDescripcion(pb.getDescripcion());
        pbr.setPrioridad(pb.getPrioridad());
        pbr.setDuracion(pb.getDuracion());
        pbr.setIdSprint(pb.getIdSprint());
        pbr.setIdUsuario(userConverter.converterUserObject(pb.getIdUsuario()));
        pbr.setEncargado(userConverter.converterUserObject(pb.getEncargado()));
        return Arrays.asList(pbr);
    }
    
    public ProductBacklog requestToEntity(ProductBacklogRequest data, Sprint sprint, Usuario user, Usuario manager){
        ProductBacklog pb = new ProductBacklog();
        pb.setIdProduct(null);
        pb.setDescripcion(data.getDescripcion());
        pb.setPrioridad(data.getPrioridad());
        pb.setDuracion(data.getDuracion());
        pb.setIdSprint(sprint);
        pb.setIdUsuario(user);
        pb.setEncargado(manager);
        return pb;
    }
    
    public ProductBacklog requestToEntityUpdate(ProductBacklogRequest data, Sprint sprint, Usuario user, Usuario manager){
        ProductBacklog pb = new ProductBacklog();
        pb.setIdProduct(data.getIdProduct());
        pb.setDescripcion(data.getDescripcion());
        pb.setPrioridad(data.getPrioridad());
        pb.setDuracion(data.getDuracion());
        pb.setIdSprint(sprint);
        pb.setIdUsuario(user);
        pb.setEncargado(manager);
        return pb;
    }
}
