/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.ingesof.converter;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import py.com.ingesof.entity.Rol;
import py.com.ingesof.entity.Usuario;
import py.com.ingesof.repository.RolRepository;
import py.com.ingesof.request.UserRequest;
import py.com.ingesof.response.RoleResponse;
import py.com.ingesof.response.UserResponse;

/**
 *
 * @author usuario
 */
@Component
public class UserConverter {
    
    @Autowired
    private RolRepository rolRepository;
    
    public List<UserResponse> converterResponseGet(List<Usuario> userList){
        ArrayList<UserResponse> userResponseList = new ArrayList<>();
        
        userList.stream().forEach(p -> {
            UserResponse userResponse = new UserResponse();
            userResponse.setIdUsuario(p.getIdUsuario());
            userResponse.setNombre(p.getNombre());
            userResponse.setApellido(p.getApellido());
            userResponse.setCorreo(p.getCorreo());
            userResponse.setTelefono(p.getTelefono());
            userResponse.setUsuario(p.getUsuario());
            userResponse.setContrasenha(p.getContrasenha());
            userResponse.setRol(new RoleResponse(p.getIdRol().getIdRol(), p.getIdRol().getNombreRol(), p.getIdRol().getDescripcion()));
            userResponseList.add(userResponse);
        });
        return userResponseList;
    }
    
    public UserResponse converterUserObject(Usuario user){
        UserResponse userResponse = new UserResponse();
        userResponse.setIdUsuario(user.getIdUsuario());
        userResponse.setNombre(user.getNombre());
        userResponse.setApellido(user.getApellido());
        userResponse.setCorreo(user.getCorreo());
        userResponse.setTelefono(user.getTelefono());
        userResponse.setUsuario(user.getUsuario());
        userResponse.setContrasenha(user.getContrasenha());
        userResponse.setRol(new RoleResponse(user.getIdRol().getIdRol(), user.getIdRol().getNombreRol(), user.getIdRol().getDescripcion()));
        return userResponse;
    }
    
    public Usuario convertFromReqToUser(UserRequest response){
        Usuario usuario = new Usuario();
        usuario.setUsuario(response.getNickName());
        usuario.setNombre(response.getFirstName());
        usuario.setApellido(response.getLastName());
        usuario.setContrasenha(response.getPassword());
        usuario.setCorreo(response.getMail());
        Optional<Rol> rolOptional = rolRepository.findById(response.getIdRol());
        usuario.setIdRol(rolOptional.get());
        usuario.setIdUsuario(null);
        usuario.setTelefono(response.getTelephone());
        return usuario;
    }
    
}
