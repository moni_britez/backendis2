/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.ingesof.converter;

import py.com.ingesof.entity.Proyecto;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import py.com.ingesof.entity.Usuario;
import py.com.ingesof.request.ProyectoRequest;
import py.com.ingesof.response.ProjectResponse;

/**
 *
 * @author usuario
 */
@Component
public class ProjectConverter {
    
    @Autowired
    private UserConverter userConverter;
    
    public Proyecto converterRequestPost(ProyectoRequest projectRequest, Usuario user) throws ParseException {
        Proyecto project = new Proyecto();
        Date dateInit = new SimpleDateFormat("dd/MM/yyyy").parse(projectRequest.getFechaInicio());
        Date dateEnd = new SimpleDateFormat("dd/MM/yyyy").parse(projectRequest.getFechaFin());  
        project.setIdProyecto(null);
        project.setFechaInicio(dateInit);
        project.setFechaFin(dateEnd);
        project.setDescripcion(projectRequest.getDescripcion());
        project.setEstado(projectRequest.getEstado());
        project.setIdUsuario(user);
        return project;
    }
    
    public ProjectResponse converterResponse(Proyecto proyecto){
        ProjectResponse projectResponse = new ProjectResponse();
        projectResponse.setDescripcion(proyecto.getDescripcion());
        projectResponse.setEstado(proyecto.getEstado());
        projectResponse.setFechaFin(proyecto.getFechaFin());
        projectResponse.setFechaInicio(proyecto.getFechaInicio());
        projectResponse.setIdProyecto(proyecto.getIdProyecto());
        projectResponse.setCreador(userConverter.converterUserObject(proyecto.getIdUsuario()));
        return projectResponse;
    }
    
    public List<ProjectResponse> converterResponseList(List<Proyecto> proyectos){
        List<ProjectResponse> projectList = new ArrayList<>();
        if(proyectos == null){
            return projectList;
        }
        for(Proyecto proyecto : proyectos){
            projectList.add(converterResponse(proyecto));
        }
        return projectList;
    }
    
    public List<ProjectResponse> converterResponseGet(List<Proyecto> project){
        List<ProjectResponse> projectResult = new ArrayList<>();
        project.stream().forEach(p -> {
            ProjectResponse projectResponse = new ProjectResponse();
            projectResponse.setIdProyecto(p.getIdProyecto());
            projectResponse.setFechaInicio(p.getFechaInicio());
            projectResponse.setFechaFin(p.getFechaFin());
            projectResponse.setDescripcion(p.getDescripcion());
            projectResponse.setEstado(p.getEstado());
            projectResult.add(projectResponse);
         });
        return projectResult;
    }
    
    public Proyecto modelToEntityProject(ProyectoRequest request, Usuario user) throws ParseException{
        Proyecto p = new Proyecto();
        p.setIdUsuario(user);
        p.setIdProyecto(request.getIdProyecto());
        p.setDescripcion(request.getDescripcion());
        p.setEstado(request.getEstado());
        p.setFechaInicio(new SimpleDateFormat("dd/MM/yyyy").parse(request.getFechaInicio()));
        p.setFechaFin(new SimpleDateFormat("dd/MM/yyyy").parse(request.getFechaFin()));
        p.setSprintList(null);
        return p;
    }
    
}
