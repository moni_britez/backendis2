package py.com.ingesof.repository;

import java.util.Collection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Repository;
import py.com.ingesof.entity.Proyecto;

@Repository
public interface ProjectRepository extends JpaRepository<Proyecto, Integer>{
    
    @Async
    @Query( value= "SELECT * FROM Proyecto P WHERE P.id_usuario = ?1",
            nativeQuery = true)
    public Collection<Proyecto> getProject(Integer idUser);
    
    @Modifying
    public void deleteByIdProyecto(Integer projectId);
}
