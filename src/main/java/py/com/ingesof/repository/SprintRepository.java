/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.ingesof.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import py.com.ingesof.entity.Sprint;

@Repository
public interface SprintRepository extends JpaRepository<Sprint, Integer>{
    
    @Query("SELECT S FROM Sprint S WHERE S.idUsuario.idUsuario = :u")
    public List<Sprint> findByIdUsuario(@Param("u") Integer idUsuario);

}
