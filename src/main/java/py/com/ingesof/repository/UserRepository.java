/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.ingesof.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import py.com.ingesof.entity.Usuario;

@Repository
public interface UserRepository extends JpaRepository<Usuario, Integer>{
    
    
    @Query("SELECT U FROM Usuario U WHERE U.usuario = :r AND U.contrasenha = :q")
    public Usuario login(@Param("r") String usuario, @Param("q") String password);
    
}