/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.ingesof.repository;

import java.util.Collection;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Repository;
import py.com.ingesof.entity.ProductBacklog;

@Repository
public interface ProductBacklogRepository extends JpaRepository<ProductBacklog, Integer>{
    
    @Async
    @Query(value="SELECT * FROM Product_backlog P WHERE P.id_usuario = ?1 ",
           nativeQuery=true)
    public List<ProductBacklog> getProductBacklogUser(Integer idUser);
    
    @Async
    @Query(value="SELECT * FROM Product_backlog P WHERE P.encargado = ?1 ",
           nativeQuery=true)
    public Collection<ProductBacklog> getProductBacklogHandle(Integer idHandle);
    
}
