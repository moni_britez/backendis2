/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.ingesof.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author usuario
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SprintRequest {
    
    private Integer idSprint;
    
    private String nombre;
    
    private String descripcion;
    
    private String estado;
    
    private Integer duracion;
    
    private Integer idProyecto;
    
    private Integer idUsuario;
    
}
