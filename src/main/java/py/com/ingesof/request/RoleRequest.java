/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.ingesof.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author usuario
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RoleRequest {
    
    @JsonProperty(value="nombre_rol")
    private String roleName;
    
    @JsonProperty(value="descripcion")
    private String description;
}
