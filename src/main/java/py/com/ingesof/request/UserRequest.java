/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.ingesof.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author usuario
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserRequest implements Serializable{
    
    @JsonProperty(value="nombre")
    private String firstName;
    
    @JsonProperty(value="apellido")
    private String lastName;
    
    @JsonProperty(value="correo")
    private String mail;
    
    @JsonProperty(value="usuario")
    private String nickName;
    
    @JsonProperty(value="contrasenha")
    private String password;
    
    @JsonProperty(value="telefono")
    private Integer telephone;
    
    @JsonProperty(value="id_rol")
    private Integer idRol;

}
