package py.com.ingesof.request;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductBacklogRequest implements Serializable{
    
    private Integer idProduct;
    
    private String descripcion;
    
    private String prioridad;
    
    private Integer duracion;
    
    private Integer idSprint;
    
    private Integer idUsuario;
    
    private Integer encargado;
}
