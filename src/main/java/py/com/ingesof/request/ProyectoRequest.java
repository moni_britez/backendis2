package py.com.ingesof.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProyectoRequest {
    
    public Integer idProyecto;
    public String fechaInicio;
    public String fechaFin;
    public String descripcion;
    public String estado;
    public Integer idUsuario;

}
