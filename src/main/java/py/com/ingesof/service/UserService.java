package py.com.ingesof.service;

import java.util.Arrays;
import py.com.ingesof.entity.Usuario;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import py.com.ingesof.converter.UserConverter;
import py.com.ingesof.repository.UserRepository;
import py.com.ingesof.request.UserRequest;
import py.com.ingesof.response.PutUserResponse;
import py.com.ingesof.response.UserPostResponse;
import py.com.ingesof.response.UserResponse;

/**
 *
 * @author usuario
 */
@Service
public class UserService {
    
    @Autowired
    private UserRepository userRepository; 
    
    @Autowired
    private UserConverter userConverter;
    
    public UserService() {}
    
    public List<UserResponse> allListUser(){            
        List<Usuario> userList = userRepository.findAll();
        List<UserResponse> userResponseList = userConverter.converterResponseGet(userList);
        return userResponseList;
    }
    
    public UserResponse getAUser(Integer id){    
        Optional<Usuario> user = userRepository.findById(id);
        UserResponse userRes = userConverter.converterUserObject(user.get());
        return userRes;
    }
    
    public List<UserPostResponse> userSave(UserRequest user) {
        Usuario usuario = userRepository.save(userConverter.convertFromReqToUser(user));
        return Arrays.asList(new UserPostResponse(usuario.getIdUsuario()));
    }
    
    public List<PutUserResponse> userUpdate(Integer id, UserRequest user) {
        Usuario usuario = userConverter.convertFromReqToUser(user);
        usuario.setIdUsuario(id); UserResponse ur = this.getAUser(id);
        userRepository.save(usuario);
        return Arrays.asList(new PutUserResponse(ur, user));
    }

}