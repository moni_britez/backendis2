package py.com.ingesof.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import py.com.ingesof.converter.ProductBacklogConverter;
import py.com.ingesof.entity.ProductBacklog;
import py.com.ingesof.entity.Sprint;
import py.com.ingesof.entity.Usuario;
import py.com.ingesof.repository.ProductBacklogRepository;
import py.com.ingesof.repository.SprintRepository;
import py.com.ingesof.repository.UserRepository;
import py.com.ingesof.request.ProductBacklogRequest;
import py.com.ingesof.response.ProductBacklogResponse;
import py.com.ingesof.util.DeteteException;

@Service
public class ProductBacklogService {
    
    @Autowired
    private ProductBacklogRepository productBacklogRepository;
    
    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private SprintRepository sprintRepository;
    
    @Autowired
    private ProductBacklogConverter productBacklogConverter;
    
    public ProductBacklogService(){}
    
    public List<ProductBacklogResponse> getAll(){
        return productBacklogConverter.entityToList(productBacklogRepository.findAll());
    }
    
    public List<ProductBacklogResponse> getFindId(Long idBackLog){
        Optional<ProductBacklog> optional = productBacklogRepository.findById(idBackLog.intValue());
        return optional.isPresent() ? productBacklogConverter.productBacklogById(optional.get()) : new ArrayList<>() ;
    }
    
    public List<ProductBacklogResponse> getProductBacklogForUserId(Long idUser){
        List<ProductBacklog> collection = productBacklogRepository.getProductBacklogUser(idUser.intValue());
        List<ProductBacklogResponse> productBacklogList =  productBacklogConverter.entityToList(collection);
        return productBacklogList;
    }
    
    public List<ProductBacklog> getProductBacklogForHandleId(Long idUser){
        Collection<ProductBacklog> collection = productBacklogRepository.getProductBacklogHandle(idUser.intValue());
        List<ProductBacklog> productBacklogList =  Collections.list(Collections.enumeration(collection));
        return productBacklogList;
    }
    
    public ProductBacklog productBacklogSave(ProductBacklogRequest data){
        Optional<Usuario> user   = userRepository.findById(data.getIdUsuario());
        Optional<Usuario> manager = userRepository.findById(data.getEncargado());
        if(user.isPresent() || manager.isPresent()){
            Optional<Sprint> sprint = sprintRepository.findById(data.getIdSprint());
            ProductBacklog pb = productBacklogConverter.requestToEntity(data, sprint.get(), user.get(), manager.get());
            return productBacklogRepository.save(pb);
        }else{
            return null;
        }
    }
    
    public ProductBacklog productBacklogUpdate(ProductBacklogRequest data){
        Optional<Usuario> user   = userRepository.findById(data.getIdUsuario());
        Optional<Usuario> manager = userRepository.findById(data.getEncargado());
        if(user.isPresent() || manager.isPresent()){
            Optional<Sprint> sprint = sprintRepository.findById(data.getIdSprint());
            ProductBacklog pb = productBacklogConverter.requestToEntityUpdate(data, sprint.get(), user.get(), manager.get());
            return productBacklogRepository.save(pb);
        }else{
            return null;
        }
    }
    
    public void deleteProduckBacklog(Long idProduck) throws DeteteException{
        try {
            productBacklogRepository.deleteById(idProduck.intValue());
        } catch (Exception e) {
            throw new DeteteException(e, HttpStatus.FORBIDDEN);
        }
    }
}
