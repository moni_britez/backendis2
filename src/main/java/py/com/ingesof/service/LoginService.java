package py.com.ingesof.service;

import py.com.ingesof.model.LoginRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import py.com.ingesof.converter.UserConverter;
import py.com.ingesof.entity.Usuario;
import py.com.ingesof.repository.UserRepository;
import py.com.ingesof.response.UserResponse;

@Service
public class LoginService {
    
    @Autowired
    private UserRepository repository;
    
    @Autowired
    private UserConverter userConverter;
    
    public UserResponse login(LoginRequest usuario){
        Usuario user = repository.login(usuario.getUsuario(), usuario.getPassword());
        return userConverter.converterUserObject(user);
    }
}
