/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.ingesof.service;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import py.com.ingesof.converter.RoleConverter;
import py.com.ingesof.entity.Rol;
import py.com.ingesof.repository.RolRepository;
import py.com.ingesof.request.RoleRequest;
import py.com.ingesof.response.PutRoleResponse;
import py.com.ingesof.response.RolePostResponse;
import py.com.ingesof.response.RoleResponse;

/**
 *
 * @author usuario
 */
@Service
public class RolService {
    
    @Autowired
    private RoleConverter roleConverter;
    
    @Autowired
    private RolRepository rolRepository;
    
    public RolService (){};
    
    public List<RoleResponse> allListRole(){    
        List<Rol> rolList = rolRepository.findAll();
        List<RoleResponse> roleResponseList = roleConverter.roleListConverter(rolList);
        return roleResponseList;
    }
    
    public RoleResponse getARole(Integer id){    
        Optional<Rol> rol = rolRepository.findById(id);
        RoleResponse roleRes = roleConverter.roleObjectConverter(rol.get());
        return roleRes;
    }
    
    public RolePostResponse roleSave(RoleRequest role) {
        Rol rol = roleConverter.convertReqToRole(role);
        rolRepository.save(rol);
        RolePostResponse rs = roleConverter.convertRolToPostResponse(rol);
        return rs;
    }
    
    public PutRoleResponse updateRole(Integer idRole, RoleRequest roleRequest) {
        Optional<Rol> rolOptional = rolRepository.findById(idRole);
        Rol rol = rolOptional.get(); rol.setIdRol(idRole);
        rolRepository.save(rol);
        RoleResponse roleBefore = roleConverter.roleObjectConverter(rol);
        return new PutRoleResponse(roleBefore, roleRequest);
    }
    
    public RolePostResponse deleteRole(Integer idRole){
        Optional<Rol> rolOptional = rolRepository.findById(idRole);
        Rol rol = rolOptional.get(); rolRepository.delete(rol);
        return new RolePostResponse(idRole);
    }
}
