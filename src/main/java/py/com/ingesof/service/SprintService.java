package py.com.ingesof.service;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import py.com.ingesof.converter.SprintConverter;
import py.com.ingesof.entity.Proyecto;
import py.com.ingesof.entity.Sprint;
import py.com.ingesof.entity.Usuario;
import py.com.ingesof.repository.ProjectRepository;
import py.com.ingesof.repository.SprintRepository;
import py.com.ingesof.repository.UserRepository;
import py.com.ingesof.request.SprintRequest;
import py.com.ingesof.util.DeteteException;

@Service
public class SprintService {
    
    @Autowired
    private SprintRepository sprintRepository;
    
    @Autowired
    private UserRepository userRepository;
            
    @Autowired
    private ProjectRepository projectRepository;
    
    @Autowired
    private SprintConverter sprintConverter;
    
    public SprintService(){}
    
    public List<Sprint> getAll(){
        return sprintRepository.findAll();
    }
    
    public Sprint save(SprintRequest sprintRequest) {
        Optional<Usuario> user = userRepository.findById(sprintRequest.getIdUsuario());
        Optional<Proyecto> project = projectRepository.findById(sprintRequest.getIdProyecto());
        if(user.isPresent() && project.isPresent()){
            Sprint sprint = sprintConverter.modelToEntitySave(sprintRequest, user.get(), project.get());
            return sprintRepository.save(sprint);
        }else{
            return null;
        }
    }
    
    public Sprint getById(Integer idSprint){
        Optional<Sprint> sp = sprintRepository.findById(idSprint);
        return sp.isPresent() ? sp.get() : null;
    }
    
    public List<Sprint> getByUserId(Integer idUsuario){
        return sprintRepository.findByIdUsuario(idUsuario);
    }    
    
    public Sprint sprintUpdate(SprintRequest sprintRequest){
        Optional<Sprint> sprintOpt = sprintRepository.findById(sprintRequest.getIdSprint());
        if(sprintOpt.isPresent()){
            Usuario user = userRepository.findById(sprintRequest.getIdUsuario()).get();
            Proyecto project = projectRepository.findById(sprintRequest.getIdProyecto()).get();
            Sprint s = sprintConverter.modelToEntityUpdate(sprintRequest, user, project);
            s.setIdSprint(sprintRequest.getIdSprint());
            return sprintRepository.save(s);
        }else{
            return null;
        }
    }
    
    public void deleteSprint(Long idSprint) throws DeteteException{
        try {
            sprintRepository.deleteById(idSprint.intValue());
        } catch (Exception e) {
            throw new DeteteException(e, HttpStatus.FORBIDDEN);
        }
    }
    
}
