package py.com.ingesof.service;

import py.com.ingesof.converter.ProjectConverter;
import py.com.ingesof.entity.Proyecto;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import py.com.ingesof.entity.Usuario;
import py.com.ingesof.repository.ProjectRepository;
import py.com.ingesof.repository.UserRepository;
import py.com.ingesof.request.ProyectoRequest;
import py.com.ingesof.response.ProjectResponse;
import py.com.ingesof.util.DeteteException;

/**
 *
 * @author usuario
 */
@Service
public class ProjectService {
    
    @Autowired
    private ProjectRepository projectRepository;
    
    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private ProjectConverter converterProject;
    
    public ProjectService() {}
    
    public List<ProjectResponse> allListProject(){  
        List<Proyecto> project = projectRepository.findAll();
        List<ProjectResponse> projectResponse = converterProject.converterResponseList(project);
        return projectResponse;
    }
    
    public Proyecto guardarProject(ProyectoRequest project) throws ParseException {
        Optional<Usuario> optional = userRepository.findById(project.getIdUsuario());
        
        if(optional.isPresent()){
            Proyecto projectInsert = converterProject.converterRequestPost(project, optional.get());
            return projectRepository.save(projectInsert);
        }else{
            return null;
        }
        
    }

    public void deleteProject(Long idProject) throws DeteteException{
        try {
            projectRepository.deleteById(idProject.intValue());
        } catch (Exception e) {
            throw new DeteteException(e, HttpStatus.FORBIDDEN);
        }
    }    
    
    public List<ProjectResponse> getProjectById(Long idUser){
        Optional<Usuario> user = userRepository.findById(idUser.intValue());
        if(user.isPresent()){
            Collection<Proyecto> projectCollection = projectRepository.getProject(idUser.intValue());
            List<Proyecto> proyectList = Collections.list(Collections.enumeration(projectCollection));
            return converterProject.converterResponseList(proyectList);
        }else{
            return new ArrayList<>();
        }
    }
    
    public Proyecto projectUpdate(ProyectoRequest data) throws ParseException{
        Optional<Usuario> user = userRepository.findById(data.getIdUsuario());
        if(user.isPresent()){
            Proyecto p = converterProject.modelToEntityProject(data, user.get());
            return projectRepository.save(p);
        }else{
            return null;
        }
    }
}
