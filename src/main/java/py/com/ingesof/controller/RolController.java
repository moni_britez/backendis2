/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.ingesof.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import py.com.ingesof.request.RoleRequest;
import py.com.ingesof.response.RoleResponse;
import py.com.ingesof.service.RolService;
import py.com.ingesof.util.ErrorException;

/**
 *
 * @author usuario
 */

@CrossOrigin("*")
@RestController
@RequestMapping("${version-app}")
public class RolController {
    
    @Autowired
    private RolService rolService;
    
    private static final String MAIN_PATH         = "/role";
    private static final String ROLE_ALL          = "/all";
    private static final String ROLE_BY_ID_PATH   = "/{id_role}";
    private static final String ROLE_SAVE_PATH    = "/save";
    private static final String ROLE_UPDATE_PATH  = "/update";
    
    @RequestMapping(value=MAIN_PATH+ROLE_ALL, produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    public ResponseEntity<?> getAllRoles() throws ErrorException{
        try {
            List<RoleResponse> rolList = rolService.allListRole();
            return new ResponseEntity<>(rolList, HttpStatus.OK);
        } catch (Exception e) {
            throw new ErrorException(e);
        }
    }
    
    @RequestMapping(value=MAIN_PATH+ROLE_BY_ID_PATH, produces=MediaType.APPLICATION_JSON_VALUE, method=RequestMethod.GET)
    public ResponseEntity<?> getRoleById(@PathVariable(value="id_role") Integer idRole) throws ErrorException {
        try {
            return new ResponseEntity<>(rolService.getARole(idRole), HttpStatus.OK);
        } catch (EmptyResultDataAccessException erdae){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e){
            throw new ErrorException(e);
        }
   }
   
    @RequestMapping(value=MAIN_PATH+ROLE_SAVE_PATH, consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE, method=RequestMethod.POST)
    public ResponseEntity<?> userSaveData(@RequestBody RoleRequest role) throws ErrorException {
        try {
            return new ResponseEntity<>(rolService.roleSave(role), HttpStatus.CREATED);
        } catch (Exception e){
            throw new ErrorException(e);
        }
    }
    
    @RequestMapping(value=MAIN_PATH+ROLE_UPDATE_PATH+ROLE_BY_ID_PATH, consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE, method=RequestMethod.PUT)
    public ResponseEntity<?> userUpdateData(@PathVariable(value="id_role") Integer idRole,
                                            @RequestBody RoleRequest role) throws ErrorException {
        try {
            return new ResponseEntity<>(rolService.updateRole(idRole, role), HttpStatus.OK);
        } catch (EmptyResultDataAccessException erdae){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e){
            throw new ErrorException(e);
        }
    }
    
    @RequestMapping(value=MAIN_PATH+ROLE_BY_ID_PATH,  produces=MediaType.APPLICATION_JSON_VALUE, method=RequestMethod.DELETE)
    public ResponseEntity<?> userUpdateData(@PathVariable(value="id_role") Integer idRole) throws ErrorException {
        try {
            return new ResponseEntity<>(rolService.deleteRole(idRole), HttpStatus.OK);
        } catch (EmptyResultDataAccessException erdae){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e){
            throw new ErrorException(e);
        }
    }
}
