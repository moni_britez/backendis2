/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.ingesof.controller;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import py.com.ingesof.response.ErrorResponse;
import py.com.ingesof.util.DeteteException;
import py.com.ingesof.util.ErrorException;

/**
 *
 * @author usuario
 */
@ControllerAdvice
@RestController
public class ListenToErrorController {
    
    @ExceptionHandler({ErrorException.class})
    private ResponseEntity<?> errorCaptured(HttpServletRequest request, ErrorException ex){
        List<ErrorResponse> errorList = new ArrayList<>();
        ErrorResponse error = new ErrorResponse();
        error.setTimestamp(LocalDateTime.now());
        error.setUri(request.getRequestURI());
        error.setMethod(request.getMethod());
        error.setDetail(ex.getMessageDetail());
        error.setCode(HttpStatus.INTERNAL_SERVER_ERROR.toString());
        
        errorList.add(error);
        
        return new ResponseEntity<>(errorList,  HttpStatus.INTERNAL_SERVER_ERROR);
    }
    
    @ExceptionHandler({DeteteException.class})
    private ResponseEntity<?> errorCaptured(HttpServletRequest request, DeteteException de){
        List<ErrorResponse> errorList = new ArrayList<>();
        ErrorResponse error = new ErrorResponse();
        error.setTimestamp(LocalDateTime.now());
        error.setUri(request.getRequestURI());
        error.setMethod(request.getMethod());
        error.setDetail(de.getMessage());
        error.setCode(de.getStatus().toString());
        
        errorList.add(error);
        
        return new ResponseEntity<>(errorList, de.getStatus());
    }
    
}
