package py.com.ingesof.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import py.com.ingesof.entity.ProductBacklog;
import py.com.ingesof.request.ProductBacklogRequest;
import py.com.ingesof.response.ProductBacklogResponse;
import py.com.ingesof.service.ProductBacklogService;
import py.com.ingesof.util.DeteteException;
import py.com.ingesof.util.ErrorException;

@CrossOrigin("*")
@RestController
@RequestMapping("${version-app}")
public class ProductBacklogController {
    
    private static final String MAIN_ADDRESS    = "/product-backlog";
    private static final String ALL             = "/all";
    private static final String SAVE            = "/save";
    private static final String BY_ID           = "/{backlogId}";
    private static final String BY_USER_ID      = "/users/{userId}";
    private static final String BY_HANDLE_ID    = "/manager/{handleId}";
    private static final String UPDATE          = "/update";
    private static final String DELETE          = "/delete/{idProduck}";
    
    @Autowired
    private ProductBacklogService productBacklogService;
    
    @GetMapping(value=MAIN_ADDRESS+ALL, produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAllProduct() throws ErrorException{
        try {
            List<ProductBacklogResponse> l = productBacklogService.getAll();
            return !l.isEmpty() ? new ResponseEntity<>(l, HttpStatus.OK):new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception exception) {
            throw new ErrorException(exception);
        }
    }
    
    @GetMapping(value=MAIN_ADDRESS+BY_USER_ID, produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAllByUser(@PathVariable("userId") Long userId) throws ErrorException{
        try {
            List<ProductBacklogResponse> l = productBacklogService.getProductBacklogForUserId(userId);
            return !l.isEmpty() ? new ResponseEntity<>(l, HttpStatus.OK):new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception exception) {
            throw new ErrorException(exception);
        }
    }
    
    @GetMapping(value=MAIN_ADDRESS+BY_HANDLE_ID, produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAllByHandle(@PathVariable("handleId") Long handleId) throws ErrorException{
        try {
            List<ProductBacklog> l = productBacklogService.getProductBacklogForHandleId(handleId);
            return !l.isEmpty() ? new ResponseEntity<>(l, HttpStatus.OK):new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception exception) {
            throw new ErrorException(exception);
        }
    }
    
    @GetMapping(value=MAIN_ADDRESS+BY_ID, produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAllProduct(@PathVariable("backlogId") Long backlogId) throws ErrorException{
        try {
            List<ProductBacklogResponse> l = productBacklogService.getFindId(backlogId);
            return !l.isEmpty() ? new ResponseEntity<>(l, HttpStatus.OK):new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception exception) {
            throw new ErrorException(exception);
        }
    }
    
    @PostMapping(value=MAIN_ADDRESS+SAVE, consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> save(@RequestBody ProductBacklogRequest data) throws ErrorException{
        try {
            ProductBacklog pb = productBacklogService.productBacklogSave(data);
            if(pb!=null){
                return new ResponseEntity<>(pb, HttpStatus.CREATED);
            }else{
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        } catch (Exception exception) {
            throw new ErrorException(exception);
        }
    }
    
    @PutMapping(value=MAIN_ADDRESS+UPDATE, consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> update(@RequestBody ProductBacklogRequest data) throws ErrorException{
        try {
            ProductBacklog pb = productBacklogService.productBacklogUpdate(data);
            if(pb!=null){
                return new ResponseEntity<>(pb, HttpStatus.OK);
            }else{
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        } catch (Exception exception) {
            throw new ErrorException(exception);
        }
    }
    
    @DeleteMapping(value=MAIN_ADDRESS+DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> deleteProjectById(@PathVariable("idProduck") Long idProduck) throws DeteteException{
        productBacklogService.deleteProduckBacklog(idProduck);
        return new ResponseEntity<>(HttpStatus.OK);
    }
    
}
