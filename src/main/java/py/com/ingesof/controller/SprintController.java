/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.ingesof.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import py.com.ingesof.entity.Sprint;
import py.com.ingesof.request.SprintRequest;
import py.com.ingesof.service.SprintService;
import py.com.ingesof.util.DeteteException;
import py.com.ingesof.util.ErrorException;

@CrossOrigin("*")
@RestController
@RequestMapping("${version-app}")
public class SprintController {
    
    private static final String MAIN_PATH = "/sprint";
    private static final String ALL       = "/all";
    private static final String ID        = "/{idUsuario}";
    private static final String SAVE      = "/save";
    private static final String UPDATE    = "/update";
    private static final String DELETE    = "/delete/{idSprint}";
    
    @Autowired
    private SprintService sprintService;
    
    @GetMapping(value=MAIN_PATH+ALL, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAll() throws ErrorException{
        try{
            List<Sprint> sprint = sprintService.getAll();
            return new ResponseEntity<>(sprint, HttpStatus.OK);
        }
        catch(Exception ex){
            throw new ErrorException(ex);
        }
    }
    
    @PostMapping(value=MAIN_PATH+SAVE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> sprintSave(@RequestBody SprintRequest sprintRequest) throws ErrorException{
        try {
            Sprint s = sprintService.save(sprintRequest);
            if(s!=null){
                return new ResponseEntity<>(s, HttpStatus.CREATED);
            }else{
                return new ResponseEntity<>(HttpStatus.PRECONDITION_FAILED);
            }
        } catch (Exception ex) {
            throw new ErrorException(ex);
        }
    }
    
    @GetMapping(value=MAIN_PATH+ID, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getSpringByUserId(@PathVariable Integer idUsuario) throws ErrorException{
        try {
            List<Sprint> sp = sprintService.getByUserId(idUsuario);
            if(sp != null){
                return new ResponseEntity<>(sp, HttpStatus.OK);
            }else{
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception ex) {
            throw new ErrorException(ex);
        }
    }
    
    @PutMapping(value=MAIN_PATH+UPDATE)
    public ResponseEntity<?> sprintUpdate(@RequestBody SprintRequest sprintRequest) throws ErrorException{
        try {
            Sprint s = sprintService.sprintUpdate(sprintRequest);
            if(s!=null){
                return new ResponseEntity<>(s, HttpStatus.OK);
            }else{
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
            
        } catch (Exception ex) {
            throw new ErrorException(ex);
        }
    }
    
    @DeleteMapping(value=MAIN_PATH+DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> deleteProjectById(@PathVariable("idSprint") Long idSprint) throws DeteteException{
        sprintService.deleteSprint(idSprint);
        return new ResponseEntity<>(HttpStatus.OK);
    }
    
}
