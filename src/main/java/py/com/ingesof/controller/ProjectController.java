package py.com.ingesof.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import py.com.ingesof.entity.Proyecto;
import py.com.ingesof.request.ProyectoRequest;
import py.com.ingesof.response.ProjectResponse;
import py.com.ingesof.service.ProjectService;
import py.com.ingesof.util.DeteteException;
import py.com.ingesof.util.ErrorException;

@CrossOrigin("*")
@RestController
@RequestMapping("${version-app}")
public class ProjectController {

    private static final String MAIN_ADDRESS = "/project";
    private static final String ALL          = "/all";
    private static final String SAVE         = "/save";
    private static final String BY_ID        = "/{idUser}";
    private static final String UPDATE       = "/update";
    private static final String DELETE       = "/delete/{idProject}";

    @Autowired
    private ProjectService service;

    @GetMapping(value = MAIN_ADDRESS + ALL, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAllProject() throws ErrorException {
        try {
            return new ResponseEntity<>(service.allListProject(), HttpStatus.OK);
        } catch (Exception e) {
            throw new ErrorException(e);
        }
    }
    
    @DeleteMapping(value=MAIN_ADDRESS + DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> deleteProjectById(@PathVariable Long idProject) throws DeteteException{
        service.deleteProject(idProject);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping(value = MAIN_ADDRESS + SAVE, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> guardar(@RequestBody ProyectoRequest project) throws ErrorException {
        try {
            Proyecto p = service.guardarProject(project);
            if (p != null) {
                return new ResponseEntity<>(p, HttpStatus.CREATED);
            } else {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }

        } catch (Exception e) {
            throw new ErrorException(e);
        }
    }

    @GetMapping(value = MAIN_ADDRESS + BY_ID, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getProjectForId(@PathVariable Long idUser) throws ErrorException {
        try {
            List<ProjectResponse> projectList = service.getProjectById(idUser);
            return !projectList.isEmpty() ? new ResponseEntity<>(projectList, HttpStatus.OK) : new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            throw new ErrorException(e);
        }
    }

    @PutMapping(value = MAIN_ADDRESS + UPDATE, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> projectUpdate(@RequestBody ProyectoRequest data) throws ErrorException {
        try {
            Proyecto p = service.projectUpdate(data);
            if (p != null) {
                return new ResponseEntity<>(p, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            throw new ErrorException(e);
        }
    }

}
