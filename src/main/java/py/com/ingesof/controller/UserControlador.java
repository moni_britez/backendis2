/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.ingesof.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import py.com.ingesof.request.UserRequest;
import py.com.ingesof.response.UserResponse;
import py.com.ingesof.service.UserService;
import py.com.ingesof.util.ErrorException;

/**
 *
 * @author usuario
 */

@CrossOrigin("*")
@RestController
@RequestMapping("${version-app}")
public class UserControlador {
    
    private static final String MAIN_PATH        = "/user";
    private static final String USER_ALL_PATH    = "/all";
    private static final String USER_SAVE_PATH   = "/save";
    private static final String USER_UPDATE_PATH = "/update";
    private static final String USER_DELETE_PATH = "/delete";
    private static final String USER_ID_PATH     = "/{id_user}";
    
    @Autowired
    private UserService service;       

    @RequestMapping(value=MAIN_PATH+USER_ALL_PATH, produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    public ResponseEntity<?> getUserAll() throws ErrorException{
        try {
            List<UserResponse> userResponseList = service.allListUser();
            return new ResponseEntity<>(userResponseList, HttpStatus.OK);
        } catch (Exception e){
            throw new ErrorException(e);
        }
    }
    
    @RequestMapping(value=MAIN_PATH+USER_ID_PATH, produces=MediaType.APPLICATION_JSON_VALUE, method=RequestMethod.GET)
    public ResponseEntity<?> getUserById(@PathVariable(value="id_user") Integer idUser) throws ErrorException {
        try {
            return new ResponseEntity<>(service.getAUser(idUser), HttpStatus.OK);
        }catch (EmptyResultDataAccessException erdae){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e){
            throw new ErrorException(e);
        }
    }
   
    @RequestMapping(value=MAIN_PATH+USER_SAVE_PATH, consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE, method=RequestMethod.POST)
    public ResponseEntity<?> userSaveData(@RequestBody UserRequest user) throws ErrorException {
        try {
            return new ResponseEntity<>(service.userSave(user), HttpStatus.CREATED);
        } catch (Exception e){
            throw new ErrorException(e);
        }
    }
   
    @RequestMapping(value=MAIN_PATH+USER_UPDATE_PATH+USER_ID_PATH, consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE, method=RequestMethod.PUT)
    public ResponseEntity<?> userUpdateData(@PathVariable(value="id_user") Integer idUsuario,
                                            @RequestBody UserRequest userRequet) throws ErrorException {
        try {
            return new ResponseEntity<>(service.userUpdate(idUsuario, userRequet), HttpStatus.OK);
        } catch (EmptyResultDataAccessException erdae){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e){
            throw new ErrorException(e);
        }
    }
   
}