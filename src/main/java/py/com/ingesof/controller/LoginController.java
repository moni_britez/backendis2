package py.com.ingesof.controller;

import py.com.ingesof.model.LoginRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import py.com.ingesof.service.LoginService;

@CrossOrigin("*")
@RestController
@RequestMapping("/login")
public class LoginController {
     @Autowired
     LoginService service;

    @RequestMapping(
            produces = MediaType.APPLICATION_JSON_VALUE,
            method = RequestMethod.POST)
    
    public ResponseEntity<?> login(@RequestBody LoginRequest usuario) {
        return new ResponseEntity<>(service.login(usuario), HttpStatus.OK);
    }

}
