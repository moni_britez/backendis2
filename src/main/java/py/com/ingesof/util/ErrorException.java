/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.ingesof.util;

import lombok.Data;

/**
 *
 * @author usuario
 */
@Data
public class ErrorException extends Exception {
    private String messageDetail;
    
    public ErrorException(Throwable problem){
        this.messageDetail = problem.getMessage();
    }
    
}
