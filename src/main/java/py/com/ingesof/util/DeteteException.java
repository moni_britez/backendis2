package py.com.ingesof.util;

import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class DeteteException extends Exception{
    private String message;
    private HttpStatus status;
    
    public DeteteException(Throwable e, HttpStatus status){
        this.message=e.getMessage();
        this.status= status;
    }
}
