/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.ingesof.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author usuario
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(Include.NON_EMPTY)	
public class UserResponse {
    
    @JsonProperty(value="id_usuario")
    private Integer idUsuario;
    
    @JsonProperty(value="nombre")
    private String nombre;
    
    @JsonProperty(value="apellido")
    private String apellido;
    
    @JsonProperty(value="correo")
    private String correo;
    
    @JsonProperty(value="usuario")
    private String usuario;
    
    @JsonProperty(value="contraseña")
    private String contrasenha;
    
    @JsonProperty(value="telefono")
    private Integer telefono;
    
    @JsonProperty(value="rol")
    private RoleResponse rol;
    
}
