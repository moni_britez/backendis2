package py.com.ingesof.response;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import py.com.ingesof.entity.Sprint;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductBacklogResponse implements Serializable {
    
    private Integer idProduct;
    
    private String descripcion;
    
    private String prioridad;
    
    private Integer duracion;
    
    private Sprint idSprint;
    
    private UserResponse idUsuario;
    
    private UserResponse encargado;
}
