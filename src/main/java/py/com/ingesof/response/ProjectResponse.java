/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.ingesof.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author usuario
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(Include.NON_EMPTY)
public class ProjectResponse {
    
    @JsonProperty(value="fecha_inicio")
    public Date fechaInicio;
    
    @JsonProperty(value="fecha_fin")
    public Date fechaFin;
    
    @JsonProperty(value="descripcion")
    public String descripcion;
    
    @JsonProperty(value="estado")
    public String estado;
    
    @JsonProperty(value="id_proyecto")
    public Integer idProyecto;
    
    @JsonProperty(value="creador")
    public UserResponse creador;
    
}
